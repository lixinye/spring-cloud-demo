package com.lee.ribbon;


import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:stock-nacos
 * @author: CareLee
 * @create:2021-08-01 21:31:33
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
public class RibbonRandomRuleConfig {
    @Bean
    public IRule iRule(){
        return new RandomRule();
    }
}
