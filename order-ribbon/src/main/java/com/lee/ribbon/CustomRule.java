package com.lee.ribbon;

import com.netflix.client.config.IClientConfig;
import com.netflix.loadbalancer.AbstractLoadBalancerRule;
import com.netflix.loadbalancer.ILoadBalancer;
import com.netflix.loadbalancer.Server;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:stock-nacos
 * @author: CareLee
 * @create:2021-08-01 22:17:49
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
public class CustomRule extends AbstractLoadBalancerRule {

    @Override
    public Server choose(Object o) {
        ILoadBalancer loadBalancer = this.getLoadBalancer();
        // 获取当前请求的服务的实例
        List<Server>  reachableServers = loadBalancer.getReachableServers();
        int random = ThreadLocalRandom.current().nextInt(reachableServers.size());
        return reachableServers.get(random);
    }

    @Override
    public void initWithNiwsConfig(IClientConfig iClientConfig) {

    }
}
