package com.lee.sentinelorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;


/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-30 08:04:25
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@EnableDiscoveryClient
@SpringBootApplication
// 自定义负载均衡策略
/*
@RibbonClients(value = {
        @RibbonClient(name="stock-service", configuration = RibbonRandomRuleConfig.class)
})
*/
public class OrderApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext applicationContext = SpringApplication.run(OrderApplication.class);
    }


}
