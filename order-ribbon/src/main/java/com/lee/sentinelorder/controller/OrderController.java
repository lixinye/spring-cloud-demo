package com.lee.sentinelorder.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-30 08:20:10
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@RestController
public class OrderController {


    @Autowired
    RestTemplate restTemplate;

    @GetMapping(value = "/add/{str}")
    public String add(@PathVariable String str){
        System.out.println("/add/{str}!");
        String msg = restTemplate.getForObject("http://stock-service/stock/reduct/"+ str, String.class);
        return "Hello world "+ msg;
    }

    @GetMapping("/test")
    public String test() {
        System.out.println("this is a test");
        return "this is a test";
    }
}
