package com.lee.sentinelorder;
/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-30 08:04:25
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.concurrent.TimeUnit;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-30 08:04:25
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/

@SpringBootApplication
public class NacosConfigApplication {
    public static void main(String[] args) {
      ConfigurableApplicationContext applicationContext = SpringApplication.run(NacosConfigApplication.class);
        while(true){
            // 当动态配置刷新时,会更新到env中,因此每隔一秒中从env中获取配置
            String userName = applicationContext.getEnvironment().getProperty("base.name");
            String userAge = applicationContext.getEnvironment().getProperty("base.age");
            System.err.println("name: "+userName+" ; age: "+userAge);
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }
    }
}
