package com.lee.sentinelorder.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-31 12:06:21
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@RestController
@RefreshScope
@RequestMapping("/config")
public class NacosController {

    @Value("${base.name}")
    String name;
    @Value("${base.age}")
    String age;

    @GetMapping("/name")
    public String getName() {
        String res = "name : "+name;
        System.out.println(res);
        return res;
    }

    @GetMapping("/age")
    public String getAge() {
        String res = "age : "+age;
        System.out.println("age : "+age);
        return res;
    }
}
