package com.lee.produce.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-31 16:03:00
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@RestController
@RequestMapping("/produce")
public class ProduceController {

    @Value("${server.port}")
    String port;

    @RequestMapping("/reduct/{str}")
    public String reduct(@PathVariable String str){
        return " "+str+" port="+port+" produce reduct success!";
    }
}
