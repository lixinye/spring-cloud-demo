package com.lee.sentinelorder.config;

import com.alibaba.cloud.nacos.ribbon.NacosRule;
import com.netflix.loadbalancer.IRule;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-30 08:13:29
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@Configuration
public class Config {
    @LoadBalanced
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder){
        return builder.build();
    }

    @Bean
    public IRule iRule(){
        return new NacosRule();
    }
}
