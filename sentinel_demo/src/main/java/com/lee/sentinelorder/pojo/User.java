package com.lee.sentinelorder.pojo;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:stock-nacos
 * @author: CareLee
 * @create:2021-08-08 17:39:55
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@Data
public class User {
    String name;
    String age;

    public User(String name) {
        this(name,null);
    }

    public User() {
    }

    public User(String name, String age) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", age='" + age + '\'' +
                '}';
    }
}
