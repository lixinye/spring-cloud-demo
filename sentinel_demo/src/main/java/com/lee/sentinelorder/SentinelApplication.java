package com.lee.sentinelorder;
/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-30 08:04:25
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/

import com.alibaba.csp.sentinel.annotation.aspectj.SentinelResourceAspect;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;


/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-30 08:04:25
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/

@SpringBootApplication
public class SentinelApplication {
    public static void main(String[] args) {
      ConfigurableApplicationContext applicationContext = SpringApplication.run(SentinelApplication.class);
    }

    @Bean
    public SentinelResourceAspect sentinelResourceAspect(){
        return new SentinelResourceAspect();
    }
}
