package com.lee.sentinelorder.config;

import com.lee.sentinelorder.feign.CustomFeignInterceptor;
import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:stock-nacos
 * @author: CareLee
 * @create:2021-08-01 23:29:55
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 *
 * 全局配置: 当使用@Configuration 会将配置作用所以的服务提供方
 *  局部配置: 如果只想针对某一个服务进行配置,就不要加@Configuration
 *
 *
 **/
@Configuration
public class FeignConfig {

    @Bean
    public Logger.Level feignLoggerLevel(){
        return Logger.Level.FULL;
    }

    @Bean
    public CustomFeignInterceptor feignAuthRequestInterceptor(){
        return new CustomFeignInterceptor();
    }
}
