package com.lee.sentinelorder.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:stock-nacos
 * @author: CareLee
 * @create:2021-08-02 22:13:44
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@FeignClient(name = "produce-service", path = "/produce")
public interface ProduceFeignService {
    @RequestMapping("/reduct/{str}")
    String reduct(@PathVariable String str);
}
