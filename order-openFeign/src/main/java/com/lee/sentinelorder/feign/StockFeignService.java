package com.lee.sentinelorder.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:stock-nacos
 * @author: CareLee
 * @create:2021-08-01 23:14:29
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 *
 * 添加feign接口和方法
 * name: 指定调用rest接口所对应的服务名
 * path: 指定调用rest接口所在的StockController 指定@RequestMapping
 *
 **/
@FeignClient(name = "stock-service",path = "/stock")
public interface StockFeignService {
    @RequestMapping("/reduct/{str}")
    String reduct(@PathVariable("str") String str);
}
/*
*@RestController
@RequestMapping("/stock")
public class StockController {

    @Value("${server.port}")
    String port;

    @RequestMapping("/reduct/{str}")
    public String reduct(@PathVariable String str){
        return " "+str+" port="+port+" save success!";
    }
}
*/