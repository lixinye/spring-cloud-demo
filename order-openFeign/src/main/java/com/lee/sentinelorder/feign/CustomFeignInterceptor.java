package com.lee.sentinelorder.feign;

import feign.RequestInterceptor;
import feign.RequestTemplate;
import lombok.extern.slf4j.Slf4j;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:stock-nacos
 * @author: CareLee
 * @create:2021-08-02 08:00:26
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@Slf4j
public class CustomFeignInterceptor implements RequestInterceptor {

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("xxx","xxx");
        requestTemplate.query("id","id");
        log.info("feign 拦截器");
    }
}
