package com.lee.sentinelorder.controller;

import com.lee.sentinelorder.feign.ProduceFeignService;
import com.lee.sentinelorder.feign.StockFeignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p> Title: $file.className</p>
 *
 * @基本功能:
 * @program:SpringCloudDemo
 * @author: CareLee
 * @create:2021-07-30 08:20:10
 * Copyright: Copyright (c) 2040 CareLee. All rights reserved.
 **/
@RestController
public class OrderController {

    @Autowired
    StockFeignService stockFeignService;

    @Autowired
    ProduceFeignService produceFeignService;

    @GetMapping(value = "/add/{str}")
    public String add(@PathVariable String str) {
        System.out.println("add " + str);
        String msg1 = produceFeignService.reduct(str);
        String msg = stockFeignService.reduct(str);
        return "Hello world "+msg1 +"  "+ msg;
    }

    @GetMapping("/test")
    public String test() {
        System.out.println("this is a test");
        return "this is a test";
    }
}
